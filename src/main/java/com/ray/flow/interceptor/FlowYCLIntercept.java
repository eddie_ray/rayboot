package com.ray.flow.interceptor;

import com.ray.interceptor.base.AopContext;
import com.ray.interceptor.base.MetaObjectIntercept;

/**
 * 审批中心-已处理的拦截器
 *
 * @author Eddie_Ray
 * @package com.ray.flow.interceptor.FlowFormeIntercept
 * @date 2022年10月8日 上午9:48:58
 */
public class FlowYCLIntercept extends MetaObjectIntercept {

    @Override
    public String queryBefore(AopContext ac) throws Exception {
        ac.sql = "SELECT t1.* FROM flow t1 right JOIN " +
                "(select distinct region,flow_no from (SELECT region,flow_no FROM flow_node WHERE region = '" + ac.user.getStr("region") + "' and multi = 0 and type = 'APPROVAL' and is_delete = 0 and userid = '" + ac.user.getStr("userid") + "' and statu > 1 " +
                "union all SELECT region,flow_no FROM flow_node_multi WHERE region = '" + ac.user.getStr("region") + "' and is_delete = 0 and type = 'APPROVAL' and userid = '" + ac.user.getStr("userid") + "' and statu > 1) t) t2" +
                " ON t1.no = t2.flow_no and t1.region = t2.region";
        ac.sql += " where t1.region = '" + ac.user.getStr("region") + "' and t1.is_delete = 0";
        return ac.sql;
    }

}
