package com.ray.flow;

import com.ray.model.Flow;
import com.ray.util.Ret;

public interface FlowCallback {

    /**
     *审批结果回调
     * @param flow 审批流程
     * @return
     */
    Ret call(Flow flow);
}
