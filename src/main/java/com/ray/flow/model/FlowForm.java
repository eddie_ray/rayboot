package com.ray.flow.model;

public class FlowForm {

    /**
     * 表单字段类型：text 文本框，user 人员，textarea 文本域，select 下拉框，checkbox 多选框，radio 单选框，file 上传文件，datetime 日期时间
     */
    private String type;

    /**
     * 表单字段名称
     */
    private String en;

    /**
     * 表单字段中文名称
     */
    private String cn;

    /**
     * 表单字段值
     */
    private Object value;

    public String getType() {
        return type;
    }

    public FlowForm setType(String type) {
        this.type = type;
        return this;
    }

    public String getEn() {
        return en;
    }

    public FlowForm setEn(String en) {
        this.en = en;
        return this;
    }

    public String getCn() {
        return cn;
    }

    public FlowForm setCn(String cn) {
        this.cn = cn;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public FlowForm setValue(Object value) {
        this.value = value;
        return this;
    }
}
