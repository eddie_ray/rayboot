package com.ray.flow.model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Record;
import com.ray.util.JsonKit;

import java.util.List;

/**
 * 流程表单字段类型-表格
 */
public class FlowFormTable {

    /**
     * 列名及标题集合(json格式)
     */
    private JSONArray col;


    /**
     * 数据行集合(json格式)
     */
    private JSONArray row;

    public JSONArray getCol() {
        return col;
    }

    public FlowFormTable setCol(List<Record> row) {
        JSONArray jsArr = new JSONArray();
        for (Record r : row){
            jsArr.add(JsonKit.toJson(r));
        }
        this.col = jsArr;
        return this;
    }

    public JSONArray getRow() {
        return row;
    }

    public FlowFormTable setRow(List<Record> row) {
        JSONArray jsArr = new JSONArray();
        for (Record r : row){
            jsArr.add(JsonKit.toJson(r));
        }
        this.row = jsArr;
        return this;
    }
}
