package com.ray.flow.model;

/**
 * 流程表单字段类型-人员
 */
public class FlowFormUser {

    /**
     * 用户id
     */
    private String userid;

    /**
     * 用户姓名
     */
    private String username;

    public String getUserid() {
        return userid;
    }

    public FlowFormUser setUserid(String userid) {
        this.userid = userid;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public FlowFormUser setUsername(String username) {
        this.username = username;
        return this;
    }
}
