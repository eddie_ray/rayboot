package com.ray.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseSysToken<M extends BaseSysToken<M>> extends Model<M> implements IBean {

	/**
	 * ID
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * ID
	 */
	public java.lang.Integer getId() {
		return getInt("id");
	}
	
	/**
	 * 域
	 */
	public M setRegion(java.lang.String region) {
		set("region", region);
		return (M)this;
	}
	
	/**
	 * 域
	 */
	public java.lang.String getRegion() {
		return getStr("region");
	}
	
	/**
	 * 账套
	 */
	public M setAccountWrap(java.lang.String accountWrap) {
		set("account_wrap", accountWrap);
		return (M)this;
	}
	
	/**
	 * 账套
	 */
	public java.lang.String getAccountWrap() {
		return getStr("account_wrap");
	}
	
	/**
	 * 平台:1=PC,2=APP
	 */
	public M setPlatform(java.lang.Integer platform) {
		set("platform", platform);
		return (M)this;
	}
	
	/**
	 * 平台:1=PC,2=APP
	 */
	public java.lang.Integer getPlatform() {
		return getInt("platform");
	}
	
	/**
	 * 用户ID
	 */
	public M setUserid(java.lang.String userid) {
		set("userid", userid);
		return (M)this;
	}
	
	/**
	 * 用户ID
	 */
	public java.lang.String getUserid() {
		return getStr("userid");
	}
	
	/**
	 * token
	 */
	public M setToken(java.lang.String token) {
		set("token", token);
		return (M)this;
	}
	
	/**
	 * token
	 */
	public java.lang.String getToken() {
		return getStr("token");
	}
	
	/**
	 * 创建时间
	 */
	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}
	
}

