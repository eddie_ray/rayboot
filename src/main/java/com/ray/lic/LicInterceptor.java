package com.ray.lic;

import com.ray.util.Ret;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class LicInterceptor implements HandlerInterceptor {

    @Autowired
    private License license;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!license.actionVerify()){
            response.getWriter().write(Ret.fail(401).toString());
        }
        return true;
    }
}
