package com.ray.lic;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartLic {

    @Autowired
    private License license;

    @PostConstruct
    public void start(){
        if(!license.verify()) {
            System.exit(1);
        }
    }
}
