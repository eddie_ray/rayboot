package com.ray.interceptor;

import com.ray.interceptor.base.AopContext;
import com.ray.interceptor.base.MetaObjectIntercept;
import com.ray.model.Company;
import com.ray.model.Department;
import com.ray.util.Ret;

/**
 * 权限拦截器
 * @package com.ray.interceptor.CompanyIntercept
 * @author  Eddie_Ray
 * @date    2022年10月8日 上午9:48:58
 */
public class CompanyIntercept extends MetaObjectIntercept {

	@Override
	public Ret addBefore(AopContext ac) throws Exception {
		Company c = Company.dao.findFirst("select * from company where no = ? and region = ?",ac.record.getStr("no"),ac.record.getStr("region"));
		if(c!=null) {
			return Ret.fail("该域该企业编号已存在，请勿重复添加！");
		}
		return super.addBefore(ac);
	}
	
	@Override
	public Ret addAfter(AopContext ac) throws Exception {
		Department d = new Department();
		d.setRegion(ac.record.getStr("region"));
		d.setParentNo("0");
		d.setNo(ac.record.getStr("no"));
		d.setName(ac.record.getStr("name"));
		d.setIndexNum(0);
		d.save();
		return super.addAfter(ac);
	}
	
	@Override
	public Ret updateAfter(AopContext ac) throws Exception {
		Department d = Department.dao.findFirst("select * from department where region = ? and parent_no = '0'",ac.record.getStr("region"));
		d.setName(ac.record.getStr("name"));
		d.update();
		return super.updateAfter(ac);
	}
}
