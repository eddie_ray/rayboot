package com.ray.interceptor;

import com.ray.interceptor.base.AopContext;
import com.ray.interceptor.base.MetaObjectIntercept;
import com.ray.util.Ret;

/**
 * 角色管理拦截器
 * @package com.ray.interceptor.RoleInterceptor
 * @author  Eddie_Ray
 * @date    2022年9月27日 上午10:44:28
 */
public class RoleInterceptor extends MetaObjectIntercept {
	
	@Override
	public String queryBefore(AopContext ac) throws Exception {
		ac.sql += " and region = '"+ac.user.getRegion()+"'";
		return ac.sql;
	}
	
	@Override
	public Ret addBefore(AopContext ac) throws Exception {
		ac.record.set("region", ac.user.getRegion());
		return super.addBefore(ac);
	}
	
}
