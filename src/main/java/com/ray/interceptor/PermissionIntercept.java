package com.ray.interceptor;

import com.ray.interceptor.base.AopContext;
import com.ray.interceptor.base.MetaObjectIntercept;
import com.ray.model.Permissions;
import com.ray.util.Ret;

/**
 * 权限拦截器
 * @package com.ray.common.interceptor.PermissionIntercept
 * @author  Eddie_Ray
 * @date    2022年10月8日 上午9:48:58
 */
public class PermissionIntercept extends MetaObjectIntercept {

	@Override
	public Ret addBefore(AopContext ac) throws Exception {
		Permissions p = Permissions.dao.findFirst("select * from permissions where no = ?",ac.record.getStr("no"));
		if(p!=null) {
			return Ret.fail("权限编号已存在，请勿重复添加！");
		}
		ac.record.set("is_auto", 0);
		return super.addBefore(ac);
	}

	@Override
	public Ret deleteBefore(AopContext ac) throws Exception {
		return Ret.fail("不能删除权限标识，请联系管理员！");
	}
}
