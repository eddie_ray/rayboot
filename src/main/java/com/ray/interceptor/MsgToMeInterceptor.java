package com.ray.interceptor;


import com.ray.interceptor.base.AopContext;
import com.ray.interceptor.base.BaseMetaObjectIntercept;

/**
 *    给我的通知列表
 * @package com.ray.interceptor.MsgToMeInterceptor
 * @author  Eddie_Ray
 * @date    2022年11月16日 上午10:13:18
 */
public class MsgToMeInterceptor extends BaseMetaObjectIntercept {
	@Override
	public String queryBefore(AopContext ac) throws Exception {
		ac.sql += " and userid = '"+ac.user.getUserid()+"'";
		return super.queryBefore(ac);
	}
}
