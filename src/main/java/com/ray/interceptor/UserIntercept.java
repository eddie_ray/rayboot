package com.ray.interceptor;

import com.jfinal.plugin.activerecord.Db;
import com.ray.interceptor.base.AopContext;
import com.ray.interceptor.base.BaseMetaObjectIntercept;
import com.ray.model.User;
import com.ray.util.Commen;
import com.ray.util.Ret;

/**
 * 权限拦截器
 * @package com.ray.common.interceptor.UserIntercept
 * @author  Eddie_Ray
 * @date    2022年10月8日 上午9:48:58
 */
public class UserIntercept extends BaseMetaObjectIntercept {

	@Override
	public Ret addBefore(AopContext ac) throws Exception {
		User u = User.dao.findFirst("select * from User where userid = ? and region = ?",ac.record.getStr("userid"),ac.user.getStr("region"));
		if(u!=null) {
			return Ret.fail("用户名已存在，请勿重复添加！");
		}
		if(ac.record.get("department_no")==null) {
			return Ret.fail("请选择左侧部门再进行用户添加！");
		}
		ac.record.set("department_no", ac.record.getStr("department_no").replaceAll("%", ""));
		ac.record.set("keywords", ac.record.get("username")
				+"|"+ac.record.get("userid")
				+"|"+ Commen.hanziToPinyin(ac.record.getStr("username"), "")
				+"|"+Commen.getPinYinHeadChar(ac.record.getStr("username")));
		ac.record.set("region",ac.user.getStr("region"));
		return super.addBefore(ac);
	}
	
	@Override
	public Ret updateBefore(AopContext ac) throws Exception {
		ac.record.set("keywords", ac.record.get("username")
				+"|"+ac.record.get("userid")
				+"|"+Commen.hanziToPinyin(ac.record.getStr("username"), "")
				+"|"+Commen.getPinYinHeadChar(ac.record.getStr("username")));
		return super.updateBefore(ac);
	}
	
	@Override
	public Ret deleteAfter(AopContext ac) throws Exception {
		Db.use(base).delete("delete from user_role where userid = ?",ac.record.getStr("userid"));
		return super.deleteAfter(ac);
	}
	
}
