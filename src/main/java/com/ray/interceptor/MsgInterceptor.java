package com.ray.interceptor;


import com.ray.interceptor.base.AopContext;
import com.ray.interceptor.base.BaseMetaObjectIntercept;

/**
 * 发文管理
 * @package com.ray.common.interceptor.MsgInterceptor
 * @author  Eddie_Ray
 * @date    2022年11月16日 上午10:13:18
 */
public class MsgInterceptor extends BaseMetaObjectIntercept {
	@Override
	public String queryBefore(AopContext ac) throws Exception {
		ac.sql += " and create_user_id = '"+ac.user.getUserid()+"'";
		return super.queryBefore(ac);
	}
}
