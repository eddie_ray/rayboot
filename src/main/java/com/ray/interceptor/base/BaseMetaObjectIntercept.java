package com.ray.interceptor.base;

import com.ray.util.Ret;

import java.util.Date;

/**
 * 公共元对象业务拦截器
 * <pre>
 * 使用场景:
 * 全局业务 例:增删改查日志记录
 * 高频字段统一处理 例:create_time update_time ...
 * 其它更多高端玩法,请尽情的发挥想象吧!
 * </pre>
 * 
 * @author ray
 * @date 2017-7-15
 *
 */
public class BaseMetaObjectIntercept extends MetaObjectIntercept {

	@Override
	public Ret addBefore(AopContext ac) throws Exception {
		ac.record.set("region", ac.user.getRegion());
		ac.record.set("create_user_id", ac.user.getStr("userid"));
		ac.record.set("create_user_name", ac.user.getStr("username"));
		return super.addBefore(ac);
	}

	@Override
	public String queryBefore(AopContext ac) throws Exception {
		ac.sql += " and region = '"+ac.user.getRegion()+"' and is_delete = 0";
		return ac.sql;
	}
	
	@Override
	public Ret updateBefore(AopContext ac) throws Exception {
		ac.record.set("update_time", new Date());
		return super.updateBefore(ac);
	}

}
