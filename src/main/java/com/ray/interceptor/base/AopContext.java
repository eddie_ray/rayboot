/**
 * Copyright (c) 2013-2016, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.ray.interceptor.base;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Record;
import com.ray.model.DataObject;
import com.ray.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * AOP 上下游
 *
 * @author ray
 * @date 2017-7-15
 */
public class AopContext {


    /**
     * 当前用户对象
     */
    public User user;
    
    /**
     * 当前元对象(object.fields=元字段集吿)
     */
    public DataObject object;

    /**
     * 当前操作数据雿(批量操作)
     */
    public List<Record> records;

    /**
     * 当前操作对象(单条数据操作)
     */
    public Record record;
    
    /**
     * 前端传递到后端的所有数据(单条数据操作)
     */
    @SuppressWarnings("rawtypes")
	public Map map;
    
    /**
     * 主子表父对象
     */
    public JSONObject parent;

    /**
     * 确认继续提交
     */
    public Boolean interceptorConfirm;
    
    /**
     * 当前操作对象固定倿
     * 用鿔：新增/编辑时预设固定初始忿
     * 推荐：固定初始忼，建议禁用字段使用addBefore()拦截添加倿
     */
    public Record fixed;

    /**
     * 追加SQL条件
     */
	public String condition = "";
    /**
     * 自定义SQL覆盖默认查询条件
     * 格式: where xxx = xxx
     */
    public String where;
    /**
     * 自定义SQL参数
     */
    public List<Object> params = new ArrayList<Object>();
    
    /**
     * select 字典表数据筛选
     */
    public String field;
    /**
     * 自定义SQL覆盖默认排序
     * 格式: order by xxx desc
     */
    public String sort;
	/**
	 * 完全自定义整个SQL语句(可以支持任意语法,多层嵌套,多表连接查询筿)
	 */
	public String sql;
	
	/*
	 * vxetable合并行或列
	 */
	public List<Record> mergeCells;

    public AopContext(User user){
        this.user = user;
    }
    
    public AopContext(User user,String field){
        this.user = user;
        this.field = field;
    }

    public AopContext(User user,List<Record> records){
        this.records = records;
    }

    public AopContext(Record record){
        this.record = record;
    }

    public AopContext(User user,Record record){
        this.user = user;
        this.record = record;
    }
    
    public AopContext(User user,Record record, @SuppressWarnings("rawtypes") Map map, Boolean interceptorConfirm) {
        this.user = user;
        this.record = record;
        this.map = map;
        this.interceptorConfirm = interceptorConfirm;
    }
    
    public AopContext(JSONObject parent, List<Record> records){
        this.parent = parent;
        this.records = records;
    }

	public String userid() {
		return this.user.getUserid();
	}

}