package com.ray.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.json.Json;

/**
 * 将JFinal的model和Record转换成json
 */
public class JsonKit {

    public static Object toJson(Object obj){
        try {
            if("[".equals(obj.toString().substring(0,1))){
                return JSON.parseArray(Json.getJson().toJson(obj));
            }else{
                return JSONObject.parseObject(Json.getJson().toJson(obj));
            }
        }catch (Exception e){
            return obj;
        }
    }
}
