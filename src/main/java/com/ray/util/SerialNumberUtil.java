package com.ray.util;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 生成流水号工具类
 * @author Ray
 *
 */
@Component
public class SerialNumberUtil {

	@Value("${spring.datasource.base.name}")
	private String base;

	@Autowired
	private RedisTemplate redisTemplate;

	public String generator(String region,String prefix){
		//时间戳 后面拼接流水号 如果需要  可以加上时分秒
		String datetime = new SimpleDateFormat("yyyyMMdd").format(new Date());
		//这里是 Redis key的前缀，如: sys:表名:日期  如果不需要去掉表名也可以
		String key = MessageFormat.format("{0}:{1}:{2}", region, prefix, datetime);
		//查询 key 是否存在， 不存在返回 1 ，存在的话则自增加1
		Long autoID = redisTemplate.opsForValue().increment(key, 1);
		// 设置key过期时间, 保证每天的流水号从1开始
		if (autoID == 1) {
			redisTemplate.expire(key, getSecondsNextEarlyMorning(), TimeUnit.SECONDS);
		}
		//这里是 6 位id，如果位数不够可以自行修改 ，下面的意思是 得到上面 key 的 值，位数为6 ，不够的话在左边补 0 ，比如  110 会变成  000110
		String value = String.format("%06d", autoID);
		//然后把 时间戳和优化后的 ID 拼接
		String code = MessageFormat.format("{0}{1}{2}", prefix, datetime, value);
		return code;
	}
	public Long getSecondsNextEarlyMorning() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return (cal.getTimeInMillis() - System.currentTimeMillis()) / 1000;
	}


	public String generator1(String field,String region){
		Record serialNumber = Db.use(base).findFirst("select * from serial_number where region = ?",region);
		int sn = serialNumber.get(field);
		String result = new SimpleDateFormat("yyMMdd").format(new Date());
		if(sn<10){
			result += "00"+sn;
		}else if(sn>=10 && sn<100){
			result += "0"+sn;
		}else if(sn>=100){
			result += String.valueOf(sn);
		}
		serialNumber.set(field, sn+1);
		Db.use(base).update("serial_number", serialNumber);
		return result;
	}
	
	/**
	* 根据长度得到唯一编号
	*
	* @param length 长度
	* @return 唯一编号
	*/
	public static String uuid(int length) {
		UUID uid = UUID.randomUUID();
		String temp = uid.toString().replace("-", "");
		if (length > 0 && length < temp.length()) {
		temp = temp.substring(temp.length() - length);
		}
		return temp;
	}
}
