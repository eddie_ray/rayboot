package com.ray.job;

import com.jfinal.plugin.activerecord.Db;
import com.ray.model.Company;
import com.ray.model.SerialNumber;
import com.ray.quartz.AbsJob;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 每天执行
 *
 * @author Ray
 * @date 2010-08-03
 */
@Component
public class EveryDayJob extends AbsJob {

	@Value("${spring.datasource.base.name}")
	private String base;

	@Override
	protected void process(JobExecutionContext context) {
		System.out.println("每日任务,自增编号归零"+base);
		try {
			Db.use(base).update("truncate serial_number");
			List<Company> list = Company.dao.findAll();
			for (int i = 0; i < list.size(); i++) {
				SerialNumber sn = new SerialNumber();
				sn.setRegion(list.get(i).getRegion());
				sn.save();
			}
        } catch (Exception e) {
            e.printStackTrace();
		}

	}

}
