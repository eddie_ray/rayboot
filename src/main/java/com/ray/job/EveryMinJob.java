package com.ray.job;

import com.ray.quartz.AbsJob;
import org.springframework.stereotype.Component;

@Component
public class EveryMinJob extends AbsJob {
	
	protected void process(org.quartz.JobExecutionContext context) {
		System.out.println("每秒钟来一发");
	};
}
