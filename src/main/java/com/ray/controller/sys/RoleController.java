package com.ray.controller.sys;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.ray.controller.base.BaseController;
import com.ray.log.WebLog;
import com.ray.model.Roles;
import com.ray.model.UserRole;
import com.ray.util.Commen;
import com.ray.util.Ret;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

    /**
     * @note 删除角色及关联的用户及关联的权限
     * @time 2024年03月26日 13:08:10
     * @author Ray
     */
    @RequestMapping("del")
    @WebLog(description = "删除角色及关联的用户及关联的权限")
    @Transactional("baseTransactionManager")
    public Ret del(@RequestBody Roles role) {
        Db.use(base).delete("delete from user_role "
                + "where role_no = ? and region = ?", role.getRoleNo(), role.getRegion());
        Db.use(base).delete("delete from role_permission "
                + "where role_no = ? and region = ?", role.getRoleNo(), role.getRegion());
        role.delete();
        return Ret.ok("msg", "成功");
    }

    /**
     * @note 获取所有用户及已拥有该角色的用户
     * @time 2024年03月26日 13:11:24
     * @author Ray
     */
    @RequestMapping("getRoleUser")
    @WebLog(description = "获取所有用户及已拥有该角色的用户")
    public Ret getRoleUser(String role_no) {
        List<Record> list = Db.use(base).find("select * from user where is_coder = 0 and region = ?", getSessionUserRegion());
        List<Record> selectList = Db.use(base).find("select * from user_role where role_no = ?", role_no);
        String[] select = new String[selectList.size()];
        for (int i = 0; i < selectList.size(); i++) {
            select[i] = selectList.get(i).get("userid");
        }
        return Ret.ok("msg", "成功").setJson("list", list).set("select", select);
    }

    /**
     * @note 根据角色获取拥有该角色的用户
     * @time 2024年03月26日 13:14:16
     * @author Ray
     */
    @RequestMapping("getUserByRole")
    @WebLog(description = "根据角色获取拥有该角色的用户")
    public Ret getUserByRole(String role_no) {
        List<Record> list = Db.use(base).find("select * from user_role where role_no = ? and region = ?", role_no, getSessionUserRegion());
        String[] select = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            select[i] = list.get(i).get("userid");
        }
        return Ret.ok("msg", "成功").set("select", select);
    }

    /**
     * @note 给角色分配人员
     * @time 2024年03月26日 13:17:26
     * @author Ray
     */
    @RequestMapping("setRoleUser")
    @WebLog(description = "给角色分配人员")
    public Ret setRoleUser(@RequestBody JSONObject param) {
        Db.use(base).delete("delete from user_role"
                + " where role_no = ? and region = ?", param.getString("role_no"), getSessionUserRegion());
        JSONArray user_ids = param.getJSONArray("user_ids");
        if (user_ids != null) {
            for (int i = 0; i < user_ids.size(); i++) {
                JSONObject temp = user_ids.getJSONObject(i);
                UserRole ur = new UserRole();
                ur.setUserid(temp.getString("userid"));
                ur.setRoleNo(param.getString("role_no"));
                ur.setRegion(getSessionUserRegion());
                ur.save();
            }
        }
        return Ret.ok("msg", "设置成功");
    }

    /**
     * @note 获取所有角色列表
     * @time 2024年03月26日 13:22:30
     * @author Ray
     */
    @RequestMapping("getRoleList")
    @WebLog(description = "获取所有角色列表")
    public Ret getRoleList() {
        List<Roles> list = Roles.dao.find("select * from roles where region = ?", getSessionUserRegion());
        return Ret.ok("msg", "成功").setJson("list", list);
    }

    /**
     * @note 根据用户获取该用户拥有的角色列表
     * @time 2024年03月26日 13:22:58
     * @author Ray
     */
    @RequestMapping("getRoleListByUserid")
    @WebLog(description = "根据用户获取该用户拥有的角色列表")
    public Ret getRoleListByUserid(String userid) {
        List<Record> selectList = Db.use(base).find("select * from user_role WHERE userid = ? AND region = ?", userid, getSessionUserRegion());
        String[] select = new String[selectList.size()];
        for (int i = 0; i < selectList.size(); i++) {
            select[i] = selectList.get(i).get("role_no");
        }
        return Ret.ok("msg", "成功").set("list", select);
    }

    /**
     * @note 给用户分配角色
     * @time 2024年03月26日 13:25:20
     * @author Ray
     */
    @RequestMapping("updateUserRole")
    @WebLog(description = "给用户分配角色")
    @Transactional("baseTransactionManager")
    public Ret updateUserRole(@RequestBody JSONObject param) {
        Db.use(base).delete("delete from user_role where userid = ? and region = ?", param.getString("userid"), getSessionUserRegion());
        JSONArray roles = param.getJSONArray("roles");
        if (!Commen.isEmptyy(roles)) {
            UserRole ur = null;
            for (int i = 0; i < roles.size(); i++) {
                ur = new UserRole();
                ur.setRegion(getSessionUserRegion());
                ur.setUserid(param.getString("userid"));
                ur.setRoleNo(roles.getString(i));
                ur.save();
            }
        }
        return Ret.ok("msg", "分配角色成功");
    }
}
