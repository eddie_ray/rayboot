package com.ray.controller.base;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.ray.log.WebLog;
import com.ray.model.Department;
import com.ray.model.Roles;
import com.ray.util.Commen;
import com.ray.util.Ret;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 选人控件后端处理
 */
@RestController
@RequestMapping("userSelector")
public class UserSelectorController extends BaseController {

    /**
     * @note 选人控件-根据关键字搜索用户信息
     * @time 2024年03月26日 13:43:16
     * @author Ray
     */
    @RequestMapping("getUserByKeywords")
    @WebLog(description = "选人控件-根据关键字搜索用户信息")
    public Ret getUserByKeywords(String keywords) {
        String sql = "select * from user where region = '" + getSessionUserRegion() + "' and is_coder = 0";
        if (!Commen.isEmptyy(keywords)) {
            sql += " and";
            sql += " keywords like '%" + keywords + "%'";
        }
        List<Record> list = dbService.find(base, "user", sql, true);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getStr("department_no") != null) {
                String[] dptArray = list.get(i).getStr("department_no").split(",");
                String dptName = "";
                for (String dpt_no : dptArray) {
                    Department dpt = Department.dao.findFirst("select * from department where region = ? and no = ?", getSessionUserRegion(), dpt_no);
                    if (dpt != null) {
                        String dptAllName = formatDpt(dpt, dpt.getName(), getSessionUserRegion());
                        dptName += dptAllName + ",";
                    }
                }
                list.get(i).set("dptName", dptName.substring(0, dptName.length() - 1));
            } else {
                list.get(i).set("dptName", "外部人员");
            }
        }
        return Ret.ok("msg", "成功").setJson("list", list);
    }

    /**
     * @note 选人控件-获取我的部门
     * @time 2024年03月26日 13:45:20
     * @author Ray
     */
    @RequestMapping("getMydpts")
    @WebLog(description = "选人控件-获取我的部门")
    public Ret getMydpts() {
        String[] dptArray = user().getDepartmentNo().split(",");
        String dpt_nos = "";
        for (String dpt_no : dptArray) {
            dpt_nos += "'" + dpt_no + "',";
        }
        List<Department> dpts = Department.dao.find("select * from department where no in (" + dpt_nos.substring(0, dpt_nos.length() - 1) + ")");
        return Ret.ok("msg", "成功").setJson("dpts", dpts);
    }

    /**
     * @note 选人控件-根据部门编号获取子部门列表及当前部门人员
     * @time 2024年03月26日 13:49:20
     * @author Ray
     */
    @RequestMapping("getDptsByNo")
    @WebLog(description = "选人控件-根据部门编号获取子部门列表及当前部门人员")
    public Ret getDptsByNo(String no) {
        Department dpt = null;
        if (Commen.isEmptyy(no)) {
            dpt = Department.dao.findFirst("select * from department where parent_no = '0' and is_delete = 0 and region = ?", getSessionUserRegion());
        } else {
            dpt = Department.dao.findFirst("select * from department where no = ? and is_delete = 0 and region = ?", no, getSessionUserRegion());
        }
        List<Department> c_dpts = Department.dao.find("select * from department where parent_no = ? and is_delete = 0 and region = ?", dpt.getNo(), getSessionUserRegion());
        List<Record> dpt_users = Db.use(base).find("SELECT *,false as checked FROM USER WHERE FIND_IN_SET(?,department_no) and is_coder = 0 and is_delete = 0", dpt.getNo());
        return Ret.ok("msg", "成功").setJson("dpt", dpt).setJson("c_dpts", c_dpts).setJson("to_choose_user_list", dpt_users);
    }

    /**
     * @note 选人控件-获取角色列表
     * @time 2024年03月26日 13:50:59
     * @author Ray
     */
    @RequestMapping("getRoles")
    @WebLog(description = "选人控件-获取角色列表")
    public Ret getRoles() {
        List<Roles> list = Roles.dao.find("select * from roles where region = ?", getSessionUserRegion());
        return Ret.ok("msg", "成功").setJson("list", list);
    }

    /**
     * @note 选人控件-根据角色获取用户列表
     * @time 2024年03月26日 13:51:33
     * @author Ray
     */
    @RequestMapping("getUserByRole")
    @WebLog(description = "选人控件-根据角色获取用户列表")
    public Ret getUserByRole(String role_no) {
        List<Record> users = Db.use(base).find("SELECT t1.* FROM USER t1 LEFT JOIN user_role t2 ON t1.region = t2.region AND t1.userid = t2.userid WHERE t1.is_coder = 0 and t1.is_delete = 0 and t2.role_no = ?", role_no);
        return Ret.ok("msg", "成功").setJson("list", users);
    }

    /**
     * @note 选人控件-获取指定可选用户列表
     * @time 2024年03月26日 13:52:27
     * @author Ray
     */
    @RequestMapping("getUsersByIds")
    @WebLog(description = "选人控件-获取指定可选用户列表")
    public Ret getUsersByIds(@RequestBody String[] userids) {
        //String[] userids = getParaValues("userids[]");
        String ids = Arrays.stream(userids).map(String::valueOf).collect(Collectors.joining(","));
        List<Record> users = Db.use(base).find("SELECT *,false as checked FROM USER WHERE find_in_set(userid,?) and is_coder = 0 and is_delete = 0", ids);
        return Ret.ok("msg", "成功").setJson("list", users);
    }

    public static String formatDpt(Department dpt, String dptAllName, String region) {
        Department parent = Department.dao.findFirst("select * from department where region = ? and no = ?", region, dpt.getParentNo());
        if (parent == null) {
            return dptAllName;
        }
        dptAllName = parent.getName() + "-" + dptAllName;
        return formatDpt(parent, dptAllName, region);
    }

}
