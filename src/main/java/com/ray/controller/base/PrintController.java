package com.ray.controller.base;

import com.ray.log.WebLog;
import com.ray.model.Print;
import com.ray.util.Ret;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/print")
public class PrintController extends BaseController {

    /**
     * @note 根据类型获取打印模板列表
     * @time 2024年03月26日 14:38:28
     * @author Ray
     */
    @RequestMapping("getList")
    @WebLog(description = "根据类型获取打印模板列表")
    public Ret getList(String type) {
        List<Print> list = Print.dao.find("select * from print where type = ? and region = ? and is_delete = 0", type, getSessionUserRegion());
        return Ret.ok("msg", "成功").setJson("list", list);
    }

    /**
     * @note 根据编号获取打印模板
     * @time 2024年03月26日 14:39:21
     * @author Ray
     */
    @RequestMapping("getTemplate")
    @WebLog(description = "根据编号获取打印模板")
    public Ret getTemplate(String no) {
        Print template = Print.dao.findFirst("select * from print where no = ? and region = ? and is_delete = 0", no, getSessionUserRegion());
        return Ret.ok("msg", "成功").setJson("template", template);
    }
}
