package com.ray.controller.base;

import cn.dev33.satoken.stp.StpUtil;
import com.ray.model.User;
import com.ray.services.base.DataObjectService;
import com.ray.services.base.DbService;
import com.ray.util.SerialNumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BaseController {

    @Value("${spring.datasource.base.name}")
    public String base;

    @Value("${spring.datasource.main.name}")
    public String main;

    @Value("${spring.datasource.sys.name}")
    public String sys;

    @Value("${rayboot.domin_url}")
    public String domin_url;

    @Value("${rayboot.domin_path}")
    public String domin_path;

    @Autowired
    public DataObjectService dataObjectService;

    @Autowired
    public DbService dbService;

    @Autowired
    public SerialNumberUtil serialNumberUtil;

    /**
     * 获取当前登录用户的id
     * @return
     */
    public static String getSessionUserId(){
        User user = (User) StpUtil.getSession().get("user");
        return user.getUserid();
    }

    /**
     * 获取当前登录用户的username
     * @return
     */
    public static String getSessionUsername(){
        User user = (User) StpUtil.getSession().get("user");
        return user.getUsername();
    }

    /**
     * 获取当前登录用户的region
     * @return
     */
    public static String getSessionUserRegion(){
        User user = (User) StpUtil.getSession().get("user");
        return user.getRegion();
    }

    /**
     * 获取当前登录用户信息
     * @return
     */
    public static User user(){
        User user = (User) StpUtil.getSession().get("user");
        return user;
    }
}
