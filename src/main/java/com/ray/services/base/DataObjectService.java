package com.ray.services.base;

import com.jfinal.plugin.activerecord.Db;
import com.ray.model.DataButton;
import com.ray.model.DataField;
import com.ray.model.DataObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 元对象数据服务
 */
@Service
public class DataObjectService {

    @Value("${spring.datasource.base.name}")
    private String base;

    /**
     * 获取元对象
     *
     * @param no 编号
     * @return
     */
    @Cacheable(value = "dataObject", key = "#no")
    public DataObject getDataObject(String no) {
        System.out.println("从数据库中获取数据dataObject");
        return DataObject.dao.findFirst("select * from data_object where no = ?", no);
    }


    /**
     * 获取元对象字段
     *
     * @param no 元对象编号
     * @return
     */
    @Cacheable(value = "dataField", key = "#no")
    public List<DataField> getDataFields(String no) {
        System.out.println("从数据库中获取数据DataField");
        return DataField.dao
                .find("select * from data_field where data_object_no = ? order by order_num", no);
    }

    /**
     * 获取元对象按钮
     *
     * @param no 元对象编号
     * @return
     */
    @Cacheable(value = "dataButton", key = "#no")
    public List<DataButton> getDataButtons(String no) {
        System.out.println("从数据库中获取数据dataButton");
        return DataButton.dao
                .find("select * from data_button where data_object_no = ? order by order_num", no);
    }

    /**
     * 根据位置获取元对象按钮
     *
     * @param no       元对象编号
     * @param location 按钮位置
     * @return
     */
    @Cacheable(value = "dataButton", key = "#no+#location")
    public List<DataButton> getDataButtons(String no, int location) {
        System.out.println("从数据库中获取数据dataButton");
        return DataButton.dao.find("select * from data_button where data_object_no = "
                + "? and location = ? order by order_num", no, location);
    }

    /**
     * 更新元对象
     *
     * @param model
     */
    @CacheEvict(value = "dataObject", key = "#model.no")
    public void dataObjectUpdate(DataObject model) {
        model.update();
    }

    /**
     * 删除元对象
     *
     * @param no 元对象编号
     * @return
     */
    @CacheEvict(value = "dataObject", key = "#no")
    @Transactional
    public void dataObjectDel(String no) {
            Db.use(base).delete("delete from data_field where data_object_no = ?", no);
            Db.use(base).delete("delete from data_button where data_object_no = ?", no);
            Db.use(base).delete("delete from data_object where no = ?", no);
    }

    /**
     * 新增元对象字段
     *
     * @param dataField
     */

    @Caching(
            evict = {@CacheEvict(value = "dataField", key = "#dataField.dataObjectNo"),
                    @CacheEvict(value = "selectRadioCheckbox",allEntries = true)}
    )
    public void dataFieldSave(DataField dataField) {
        dataField.save();
    }

    /**
     * 更新元对象字段
     *
     * @param dataField
     */
    @Caching(
            evict = {@CacheEvict(value = "dataField", key = "#dataField.dataObjectNo"),
                    @CacheEvict(value = "selectRadioCheckbox",allEntries = true)}
    )
    public void dataFieldUpdate(DataField dataField) {
        dataField.update();
    }

    /**
     * 删除元对象字段
     *
     * @param dataField
     */
    @Caching(
            evict = {@CacheEvict(value = "dataField", key = "#dataField.dataObjectNo"),
                    @CacheEvict(value = "selectRadioCheckbox",allEntries = true)}
    )
    public void dataFieldDel(DataField dataField) {
        dataField.delete();
    }

    /**
     * 新增元对象字段
     *
     * @param model
     */
    @CacheEvict(value = "dataButton", key = "#model.dataObjectNo")
    public void dataButtonSave(DataButton model) {
        model.save();
    }

    /**
     * 更新元对象字段
     *
     * @param model
     */
    @CacheEvict(value = "dataButton", key = "#model.dataObjectNo")
    public void dataButtonUpdate(DataButton model) {
        model.update();
    }

    /**
     * 删除元对象字段
     *
     * @param model
     */
    @CacheEvict(value = "dataButton", key = "#model.dataObjectNo")
    public void dataButtonDel(DataButton model) {
        model.delete();
    }
}
