package com.ray.services;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {

    @Value("${spring.datasource.base.name}")
    public String base;

    @Cacheable(value = "menu",key="0")
    public List<Record> getMenus(Record menu){
        String sql = "select * from menu"
                +" where parent_menu = ? and is_hide = 0 order by seq_num";
        List<Record> children_menu = Db.use(base).find(sql,menu.getInt("id"));
        for (Record children : children_menu) {
            List<Record> temp = getMenus(children);
            children.set("children", temp);
        }
        return children_menu;
    }
}
